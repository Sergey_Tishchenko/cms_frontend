export const config = {
  serverUrl: 'http://localhost:3030',
  serverApiUrl: 'http://localhost:3030/api/v1',
  recentBlockCount: 5,
  archiveBlockCount: 30
};
