import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {RouterModule} from "@angular/router";
import {LoginComponent} from "./login.component";
import {ReactiveFormsModule} from "@angular/forms";

@NgModule({
  declarations: [LoginComponent],
  imports: [
    ReactiveFormsModule,
    BrowserModule,
    RouterModule.forChild([
      {path: 'login', component: LoginComponent}
    ])
  ]
})
export class LoginModule {}
