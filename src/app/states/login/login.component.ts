import {Component, OnInit} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../../services/auth/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  login: string;
  pass: string;
  loginForm: FormGroup;
  errorMsg: string;


  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.buildForm();
  }

  private buildForm(): void {
    this.loginForm = this.fb.group({
      login: ['', Validators.required],
      pass: ['', Validators.required]
    });

    this.loginForm.valueChanges.subscribe(data => this.errorMsg = null);
  }

  public checkError(element: string, errorType: string): boolean {
    return this.loginForm.get(element).hasError(errorType) &&
      this.loginForm.get(element).touched;
  }

  onSubmit(form: FormGroup): void {
    console.log("submit");
    console.log(this.loginForm.value);
    let user = {login: this.loginForm.value.login, pass: this.loginForm.value.pass};
    this.authService.login(user)
      .subscribe(
        res => {if (res) this.router.navigate(['/admin']); },
        err => {
          this.errorMsg = err.json().msg;
          console.log(err.json());
        });
  }
}
