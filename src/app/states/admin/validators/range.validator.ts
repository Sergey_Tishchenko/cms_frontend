import {AbstractControl, ValidatorFn} from "@angular/forms";

export function rangeValidator(minValue: number, maxValue: number): ValidatorFn {
  return function(control: AbstractControl): {[key: string]: any} {
    let value = control.value;

    if (isNaN(value)) return { 'range': {value} };

    if ((value < minValue) || (value > maxValue)) {
      return { 'range': {value} };
    } else {
      return null;
    }
  };
}
