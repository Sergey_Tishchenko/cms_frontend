import {AbstractControl} from "@angular/forms";

export function myValidator(control: AbstractControl): {[key: string]: any} {
  let value = control.value;

  if (value === 'Sergey') {
    // no errors
    return null;
  } else {
    return {'myValidator': { value }};
  }
}
