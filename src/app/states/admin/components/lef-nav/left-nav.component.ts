import {Component, OnInit} from "@angular/core";
import {User} from "../../../../model/User";
import {Role} from "../../../../model/Role";
import {AuthService} from "../../../../services/auth/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-left-nav',
  templateUrl: './left-nav.component.html',
  styleUrls: ['./left-nav.component.css']
})
export class LeftNavComponent implements OnInit {
  curuser: User;
  constructor(
    private authService: AuthService,
    private router: Router
  ) {}

  ngOnInit(): void {
    // console.log(this.authService.getCurrentUser());
    this.curuser = this.authService.getCurrentUser();
    // console.log(this.curuser);
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['/home']);
  }
}
