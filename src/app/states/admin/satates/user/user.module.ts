import {NgModule} from '@angular/core';
import {UserListComponent} from "./components/user-list/user-list.component";
import {BrowserModule} from "@angular/platform-browser";
import {RouterModule} from "@angular/router";
import {UserCreateUpdateComponent} from "./components/user-create-update/user-create-update.component";
import {ReactiveFormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    UserListComponent,
    UserCreateUpdateComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    ReactiveFormsModule
  ]
})
export class UserModule {}
