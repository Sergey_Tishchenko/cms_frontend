import {Component, OnInit} from "@angular/core";
import {User} from "../../../../../../model/User";
import {UserService} from "../../../../../../services/user.service";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {Role} from "../../../../../../model/Role";
import {TagService} from "../../../../../../services/tag.service";
import {RoleService} from "../../../../../../services/role.service";
import {rangeValidator} from "../../../../validators/range.validator";
import {myValidator} from "../../../../validators/my.validator";
import {AlertsService} from "../../../../../../lib/jaspero-alerts/alerts.service";

@Component({
  selector: './app-user-create-update',
  templateUrl: './user-create-update.component.html',
  styleUrls: ['./user-create-update.component.css']
})
export class UserCreateUpdateComponent implements OnInit {
  user: User;
  roles: Role[];
  errorMessage: string;
  userForm: FormGroup;

  formErrors = {
    'firstName': '',
    'lastName': '',
    'login': '',
    'email': '',
    'pass': '',
    'role': ''
  };

  validationMessages = {
    'firstName': {
      'required': 'required',
      'minlength': 'must be at least 2 symbols'
    },
    'lastName': {
      'required': 'required',
      'minlength': 'must be at least 2 symbols'
    },
    'login': {
      'required': 'required',
      'minlength': 'must be at least 2 symbols'
    },
    'email': {
      'required': 'required',
      'pattern': 'wrong e-mail'
    },
    'pass': {
      'required': 'required',
      'minlength': 'must be at least 4 symbols'
    },
    'role': {
      'required': 'required'
    },
  };

  constructor(
    private tagService: RoleService,
    private userService: UserService,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private router: Router,
    private alertsService: AlertsService
  ) {}

  ngOnInit(): void {
    this.tagService.getAll().subscribe(res => this.roles = res);
    this.buildForm();
    this.getUser();
  }

  public checkError(element: string, errorType: string): boolean {
    return this.userForm.get(element).hasError(errorType) &&
        this.userForm.get(element).touched;
  }

  public onSubmit(userForm: FormGroup): void {
    // console.log(userForm.value);
    if (this.user && this.user.id) {
      // update
      this.user.firstName = userForm.value.firstName;
      this.user.lastName = userForm.value.lastName;
      this.user.login = userForm.value.login;
      this.user.email = userForm.value.email;
      this.user.pass = userForm.value.pass;
      this.user.role = userForm.value.role;

      this.userService.update(this.user).subscribe(
        res => {
          this.alertsService.create('success', 'user has been updated');
          this.goBack();
        },
        err => {
          this.alertsService.create('error', err.json().msg);
        }
      );
    } else {
      // create
      this.user = new User(null, userForm.value.firstName, userForm.value.lastName, userForm.value.login, userForm.value.email, userForm.value.pass, userForm.value.role);
      console.log(this.user);
      this.userService.create(this.user).subscribe(
        res => {
          this.alertsService.create('success', 'user has been created');
          this.goBack();
        },
        err => {
          this.alertsService.create('error', err.json().msg);
        }
      );
    }
  }

  public goBack(): void {
    this.router.navigate(['/admin/users']);
  }

  private getUser(): void {
    this.activatedRoute.params.forEach((params: Params) => {
      let id = params['id'];
      if (id) {
        this.userService.getUserById(id).subscribe(res => {
          this.user = res;
          this.userForm.patchValue(this.user);
        });
      }
    });
  }

  private buildForm(): void {

    this.userForm = this.fb.group({
      firstName: ['user1', [Validators.required, Validators.minLength(2)]],
      lastName: ['', [Validators.required, Validators.minLength(2)]],
      login: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.pattern('[a-zA-Z0-9._%-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}')]],
      pass: ['', [Validators.required, Validators.minLength(4)]],
      role: [new Role(null, null), Validators.required]
    });
    /*this.userForm = new FormGroup({
      firstName: new FormControl('user1', Validators.required),
      lastName: new FormControl('', [Validators.required, Validators.minLength(2)]),
      login: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required]),
      pass: new FormControl('', [Validators.required]),
      role: new FormControl(new Role(null, null), [Validators.required]),
    });*/

    this.userForm.valueChanges.subscribe(data => this.onValueChange(data));
    this.onValueChange();
  }

  onValueChange(data?: any) {
    // console.log("onValueChange: ");
    // console.log(data);
    if (!this.userForm) return;

    let form = this.userForm;

    for (let field in this.formErrors) {
      this.formErrors[field] = '';

      let control = form.get(field);
      // console.log(field);
      // console.log(control.errors);
      // console.log(control);

      if (control && control.dirty && !control.valid) {
        // console.log('error');
        let message = this.validationMessages[field];
        // console.log(message);
        for (let key in control.errors) {
          this.formErrors[field] += message[key] + ' ';
          // console.log(this.formErrors[field]);
        }
      }
    }
  }

  byId(item1: Role, item2: Role) {
    return item1.id === item2.id;
  }

  showError(msg: string) {}
}
