import {NgModule} from '@angular/core';
import {UserModule} from "./user/user.module";
import {TagModule} from "./tag/tag.module";
import {PostModule} from "./post/post.module";
import {RouterModule} from "@angular/router";
import {BrowserModule} from "@angular/platform-browser";

@NgModule({
  declarations: [],
  imports: [
    UserModule,
    TagModule,
    PostModule,
    RouterModule,
    BrowserModule
  ]
})
export class AppAdminStatesModule {}
