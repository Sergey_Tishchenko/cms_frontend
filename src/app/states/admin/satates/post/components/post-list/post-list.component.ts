import {Component, OnInit} from "@angular/core";
import {PostDetails} from "../../../../../../model/PostDetails";
import {PostDetailsService} from "../../../../../../services/post-details.service";


@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements OnInit {
  postDetails: PostDetails[];
  constructor(
    private postDetailsService: PostDetailsService
  ) {}

  ngOnInit(): void {
    this.postDetailsService.getAll().subscribe(res => this.postDetails = res);
  }

}
