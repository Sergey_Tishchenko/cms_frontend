import {NgModule} from '@angular/core';
import {BrowserModule} from "@angular/platform-browser";
import {PostListComponent} from "./components/post-list/post-list.component";

@NgModule({
  declarations: [
    PostListComponent
  ],
  imports: [
    BrowserModule
  ]
})
export class PostModule {}
