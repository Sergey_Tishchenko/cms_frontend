import {Component, OnInit} from "@angular/core";
import {TagService} from "../../../../../../services/tag.service";
import {Tag} from "../../../../../../model/Tag";
import {ActivatedRoute, Router} from "@angular/router";
import {AlertsService} from "../../../../../../lib/jaspero-alerts/alerts.service";


@Component({
  selector: 'app-tag-delete',
  templateUrl: './tag-delete.component.html',
  styleUrls: ['./tag-delete.component.css']
})
export class TagDeleteComponent implements OnInit {
  tag: Tag;

  constructor(
    private tagService: TagService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private alertsService: AlertsService
  ) {}

  ngOnInit(): void {
    let id = this.activatedRoute.snapshot.params['id'];
    this.tagService.getById(id).subscribe(
      res => {this.tag = res; },
      err => {console.log('err'); }
    );
  }

  cancel(): void {
    console.log('cancel');
    this.router.navigate(['/admin/tags']);
  }
  delete(): void {
    console.log('delete');
    this.tagService.delete(this.tag).subscribe(
      res => {
        this.alertsService.create('success', 'tag was deleted');
        this.router.navigate(['/admin/tags']);
        },
      err => {
        console.log(err.json());
        this.alertsService.create('error', err.json().msg);
      }
    );
  }
}
