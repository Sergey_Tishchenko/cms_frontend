import {Component, OnInit} from "@angular/core";
import {Tag} from "../../../../../../model/Tag";
import {TagService} from "../../../../../../services/tag.service";


@Component({
  selector: 'app-tag-list',
  templateUrl: './tag-list.component.html',
  styleUrls: ['./tag-list.component.css']
})
export class TagListComponent implements OnInit {
  tags: Tag[];

  constructor(
    private tagService: TagService
  ) {}

  ngOnInit(): void {
    this.tagService.getAll().subscribe(res => this.tags = res );
  }
}
