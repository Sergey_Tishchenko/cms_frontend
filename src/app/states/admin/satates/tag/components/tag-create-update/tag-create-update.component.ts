import {Component, OnInit} from "@angular/core";
import {TagService} from "../../../../../../services/tag.service";
import {Tag} from "../../../../../../model/Tag";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {AlertsService} from "../../../../../../lib/jaspero-alerts/alerts.service";

@Component({
  selector: 'app-tag-create-update',
  templateUrl: './tag-create-update.component.html',
  styleUrls: ['./tag-create-update.component.css']
})
export class TagCreateUpdateComponent implements OnInit {
  public tag: Tag;
  public tagForm: FormGroup;

  constructor(
    private tagService: TagService,
    private fb: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private alertsService: AlertsService
  ) {}

  ngOnInit(): void {
    this.buildForm();
    this.getTag();
  }

  private buildForm(): void {
    this.tagForm = this.fb.group({
      name: ['', Validators.required]
    });
  }

  public goBack(): void {
    this.router.navigate(['/admin/tags']);
  }

  public onSubmit(form: FormGroup): void {
    // console.log(form.value);
    if (this.tag && this.tag.id) {
      // update
      this.tag.name = form.value.name;
      this.tagService.update(this.tag).subscribe(
        res => {
          this.alertsService.create('success', 'tag was updated');
          this.goBack();
          },
        err => {
          let msg = '';
          if (err.json().errmsg.includes('duplicate key')) msg = 'already exists';
          this.alertsService.create('error', msg);
        }
      );
    } else {
      // create
      this.tag = new Tag(null, form.value.name);
      this.tagService.create(this.tag).subscribe(
        res => {
          this.alertsService.create('success', 'tag was created');
          this.goBack();
        },
        err => {
          let msg = '';
          if (err.json().errmsg.includes('duplicate key')) msg = 'already exists';
          this.alertsService.create('error', msg);
        }
      );
    }
  }

  public checkError(element: string, errorType: string): boolean {
    return this.tagForm.get(element).hasError(errorType) &&
      this.tagForm.get(element).touched;
  }

  private getTag() {
    this.activatedRoute.params.forEach((params: Params) => {
      let id = params['id'];
      if (id) {
        this.tagService.getById(id).subscribe(res => {
          this.tag = res;
          this.tagForm.patchValue(this.tag);
        });
      }
    });
  }
}
