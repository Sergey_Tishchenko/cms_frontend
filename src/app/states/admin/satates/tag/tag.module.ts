import {NgModule} from '@angular/core';
import {BrowserModule} from "@angular/platform-browser";
import {TagListComponent} from "./components/tag-list/tag-list.component";
import {TagCreateUpdateComponent} from "./components/tag-create-update/tag-create-update.component";
import {ReactiveFormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";
import {TagDeleteComponent} from "./components/tag-delete/tag-delete.component";

@NgModule({
  declarations: [
    TagListComponent,
    TagCreateUpdateComponent,
    TagDeleteComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    ReactiveFormsModule
  ]
})
export class TagModule {}
