import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {RouterModule} from "@angular/router";
import {AdminComponent} from "./admin.component";
import {AuthGuard} from "../../services/auth-guard.service";
import {LeftNavComponent} from "./components/lef-nav/left-nav.component";
import {UserListComponent} from "./satates/user/components/user-list/user-list.component";
import {AppAdminStatesModule} from "./satates/app-admin-states.module";
import {TagListComponent} from "./satates/tag/components/tag-list/tag-list.component";
import {PostListComponent} from "./satates/post/components/post-list/post-list.component";
import {UserCreateUpdateComponent} from "./satates/user/components/user-create-update/user-create-update.component";
import {TagCreateUpdateComponent} from "./satates/tag/components/tag-create-update/tag-create-update.component";
import {TagDeleteComponent} from "./satates/tag/components/tag-delete/tag-delete.component";

@NgModule({
  declarations: [
    AdminComponent,
    LeftNavComponent
  ],
  imports: [
    AppAdminStatesModule,
    BrowserModule,
    RouterModule.forChild([
      {
        path: 'admin',
        component: AdminComponent,
        canActivate: [AuthGuard],
        children: [
          {
            path: '',
            children: [
              // {path: '', redirectTo: '/users', pathMatch: 'full'},
              {path: 'users', component: UserListComponent},
              {path: 'users/create', component: UserCreateUpdateComponent},
              {path: 'users/update/:id', component: UserCreateUpdateComponent},

              {path: 'tags', component: TagListComponent},
              {path: 'tags/create', component: TagCreateUpdateComponent},
              {path: 'tags/update/:id', component: TagCreateUpdateComponent},
              {path: 'tags/delete/:id', component: TagDeleteComponent},

              {path: 'posts', component: PostListComponent},
              // {path: 'tag/:id', component: TagComponent},
              // {path: 'post/:id', component: PostComponent}
            ]
          }
        ]
      }
    ])
  ]
})
export class AdminModule {}
