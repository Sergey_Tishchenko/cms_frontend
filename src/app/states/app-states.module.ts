import {NgModule} from '@angular/core';

import {MainModule} from "./main/main.module";
import {AdminModule} from "./admin/admin.module";
import {RouterModule} from "@angular/router";
import {LoginModule} from "./login/login.module";

@NgModule({
  declarations: [],
  imports: [
    MainModule,
    AdminModule,
    LoginModule,
    RouterModule
  ]
})
export class AppStatesModule {}
