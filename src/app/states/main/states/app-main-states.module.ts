import {NgModule} from '@angular/core';
import {HomeComponent} from "./components/home/home.component";
import {ArchiveComponent} from "./components/archive/archive.component";
import {PostComponent} from "./components/post/post.component";
import {TagComponent} from "./components/tag/tag.component";
import {PostDetailsComponent} from "./components/shared/post-details/post-details.component";
import {RouterModule} from "@angular/router";
import {BrowserModule} from "@angular/platform-browser";

@NgModule({
  declarations: [
    HomeComponent,
    ArchiveComponent,
    PostComponent,
    TagComponent,
    PostDetailsComponent
  ],
  imports: [
    BrowserModule,
    RouterModule
  ]
})
export class AppMainStatesModule {}
