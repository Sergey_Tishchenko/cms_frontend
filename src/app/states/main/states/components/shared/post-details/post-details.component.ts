import {Component, Input} from '@angular/core';
import {PostDetails} from "../../../../../../model/PostDetails";

@Component({
  selector: 'app-post-details',
  templateUrl: './post-details.component.html',
  styleUrls: ['./post-details.component.css']
})
export class PostDetailsComponent {
  @Input()
  postDetails: PostDetails;
}
