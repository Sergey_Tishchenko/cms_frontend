import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params} from '@angular/router';
import {PostDetails} from "../../../../../model/PostDetails";
import {PostDetailsService} from "../../../../../services/post-details.service";

@Component({
  selector: 'app-tag',
  templateUrl: './tag.component.html'
})
export class TagComponent implements OnInit {
  postDetailsList: PostDetails[];

  constructor(
    private activatedRoute: ActivatedRoute,
    private postDetailsService: PostDetailsService
  ) {}

  ngOnInit(): void {
    this.activatedRoute.params.forEach((params: Params) => {
      const id = params['id'];
      this.postDetailsService.getByTagId(id).subscribe(res => {
        this.postDetailsList = res;
      });
    });
  }
}
