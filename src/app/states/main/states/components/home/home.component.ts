import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params} from '@angular/router';
import {PostDetails} from "../../../../../model/PostDetails";
import {PostDetailsService} from "../../../../../services/post-details.service";



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit{
  postDetailsList: PostDetails[];

  constructor(
    private activatedRoute: ActivatedRoute,
    private postDetailsService: PostDetailsService
  ) {}

  ngOnInit(): void {
    this.activatedRoute.params.forEach((params: Params) => {
      const id = params['id'];
      this.postDetailsService.getForHome().subscribe(res => {
        this.postDetailsList = res;
      });
    });
  }
}
