import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params} from '@angular/router';
import {PostDetails} from "../../../../../model/PostDetails";
import {PostDetailsService} from "../../../../../services/post-details.service";



@Component({
  selector: 'app-archive',
  templateUrl: './archive.component.html'
})
export class ArchiveComponent implements OnInit{
  postDetailsList: PostDetails[];

  constructor(
    private activatedRoute: ActivatedRoute,
    private postDetailsService: PostDetailsService
  ) {}

  ngOnInit(): void {
    this.activatedRoute.params.forEach((params: Params) => {
      const date = params['date'];
      if (date) {
        this.postDetailsService.getForArchiveWithDate(date).subscribe(res => {
          this.postDetailsList = res;
        });
      } else {
        this.postDetailsService.getForArchive().subscribe(res => {
          this.postDetailsList = res;
        });
      }
    });
  }
}
