import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params} from '@angular/router';
import {Post} from "../../../../../model/Post";
import {PostService} from "../../../../../services/post.service";


@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {
  post: Post;

  constructor(
    private activatedRoute: ActivatedRoute,
    private postService: PostService
  ) {}

  ngOnInit(): void {
    this.activatedRoute.params.forEach((params: Params) => {
     let id = params['id'];
      this.postService.getPostById(id).subscribe(res => {
        this.post = res;
      });
     });

  // let id = this.activatedRoute.snapshot.params['id'];
  //   this.post = this.postService.getPostById(id);
  }
}
