///<reference path="states/components/home/home.component.ts"/>
import {NgModule} from "@angular/core";
import {AppMainStatesModule} from "./states/app-main-states.module";
import {MainComponent} from "./main.component";
import {ArchiveBlockComponent} from "./components/archive-block/archive-block.component";
import {RecentBlockComponent} from "./components/recent-block/recent-block.component";
import {SearchBlockComponent} from "./components/search-block/search-block.component";
import {TagsBlockComponent} from "./components/tags-block/tags-block.component";
import {BrowserModule} from "@angular/platform-browser";
import {RouterModule} from "@angular/router";
import {HomeComponent} from "./states/components/home/home.component";
import {ArchiveComponent} from "./states/components/archive/archive.component";
import {TagComponent} from "./states/components/tag/tag.component";
import {PostComponent} from "./states/components/post/post.component";


@NgModule({
  declarations: [
    MainComponent,
    ArchiveBlockComponent,
    RecentBlockComponent,
    SearchBlockComponent,
    TagsBlockComponent
  ],
  imports: [
    AppMainStatesModule,
    BrowserModule,
    RouterModule.forChild([
      {
        path: '',
        component: MainComponent,
        children: [
          {
            path: '',
            children: [
              {path: '', redirectTo: '/home', pathMatch: 'full'},
              {path: 'home', component: HomeComponent},
              {path: 'archive', component: ArchiveComponent},
              {path: 'archive/:date', component: ArchiveComponent},
              {path: 'tag/:id', component: TagComponent},
              {path: 'post/:id', component: PostComponent}
            ]
          }
        ]
      }
    ])
  ]
})
export class MainModule {}
