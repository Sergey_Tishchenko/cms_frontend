import { Component, OnInit } from '@angular/core';
import {Tag} from "../../../../model/Tag";
import {TagService} from "../../../../services/tag.service";


@Component({
  selector: 'app-tags-block',
  templateUrl: './tags-block.component.html',
  styleUrls: ['./tags-block.component.css']
})
export class TagsBlockComponent implements OnInit {
  tagList: Tag[];
  constructor(
    private tagService: TagService
  ) { }

  ngOnInit() {
    this.tagService.getAll().subscribe(res => this.tagList = res);
  }

}
