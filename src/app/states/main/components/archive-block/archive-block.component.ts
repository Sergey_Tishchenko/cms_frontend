import { Component, OnInit } from '@angular/core';
import {config} from "../../../../../config/config";
import {DatesService} from "../../../../services/dates.service";


@Component({
  selector: 'app-archive-block',
  templateUrl: './archive-block.component.html',
  styleUrls: ['./archive-block.component.css']
})
export class ArchiveBlockComponent implements OnInit {
  dates: Date[];
  datesCount: number = config.archiveBlockCount;

  constructor(
    private datesService: DatesService
  ) { }

  ngOnInit() {
    this.datesService.getAll(this.datesCount).subscribe(res => {this.dates = res; });
  }

}
