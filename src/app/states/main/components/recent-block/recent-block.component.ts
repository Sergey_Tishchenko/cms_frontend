import { Component, OnInit } from '@angular/core';
import {PostInfo} from "../../../../model/PostInfo";
import {config} from "../../../../../config/config";
import {PostInfoService} from "../../../../services/post-info.service";


@Component({
  selector: 'app-recent-block',
  templateUrl: './recent-block.component.html',
  styleUrls: ['./recent-block.component.css']
})
export class RecentBlockComponent implements OnInit {
  postInfoList: PostInfo[];
  postInfoCount: number = config.recentBlockCount;

  constructor(
    private postInfoService: PostInfoService
  ) { }

  ngOnInit() {
    this.postInfoService.getAll(this.postInfoCount).subscribe(res => this.postInfoList = res);
  }

}
