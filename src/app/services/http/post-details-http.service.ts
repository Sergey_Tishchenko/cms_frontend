import {Injectable} from '@angular/core';
import {config} from '../../../config/config';
import {Http, RequestOptions, Response, URLSearchParams} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {PostDetails} from '../../model/PostDetails';
import {PostDetailsService} from "../post-details.service";
import {User} from "../../model/User";

@Injectable()
export class PostDetailsServiceHttp implements PostDetailsService {
  url = config.serverApiUrl + '/postdetails';

  constructor(
    private http: Http
  ) {}

  getByTagId(id: string): Observable<PostDetails[]> {
    const params = new URLSearchParams();
    params.set('key', 'tag');
    params.set('val', id);
    const options: RequestOptions = new RequestOptions();
    options.params = params;
    return this.http.get(this.url, options).map(this.extractPostDetails).catch(this.handleError);
  }

  getForHome(): Observable<PostDetails[]> {
    const params = new URLSearchParams();
    params.set('key', 'home');
    const options: RequestOptions = new RequestOptions();
    options.params = params;
    return this.http.get(this.url, options).map(this.extractPostDetails).catch(this.handleError);
  }

  getForArchive(): Observable<PostDetails[]> {
    const params = new URLSearchParams();
    params.set('key', 'archive');
    const options: RequestOptions = new RequestOptions();
    options.params = params;
    return this.http.get(this.url, options).map(this.extractPostDetails).catch(this.handleError);
  }

  getForArchiveWithDate(date: string) {
    const params = new URLSearchParams();
    params.set('key', 'archive');
    params.set('val', date);
    const options: RequestOptions = new RequestOptions();
    options.params = params;
    return this.http.get(this.url, options).map(this.extractPostDetails).catch(this.handleError);
  }

  getAll(): Observable<PostDetails[]> {
    return this.http.get(this.url).map(this.extractPostDetails).catch(this.handleError);
  }


  //helper function
  private extractPostDetails(res: Response): PostDetails[] {
    const list: PostDetails[] = [];
    res.json().forEach(item => {
      list.push(
        new PostDetails(item._id, item.title, new Date(item.date), config.serverUrl + item.imgSrc, config.serverUrl + item.iconSrc, item.description,
          (item.user)? new User(item.user._id, item.user.firstName, item.user.lastName, item.user.login, null, null, null) : null));
    });
    return list;
  }

  private handleError(err: any, cought: Observable<any>): any {
    let msg = '';
    if (err instanceof Response) {
      const errData = err.json().err || JSON.stringify(err.json());
      msg = `${err.status} - ${err.statusText || ''} ${errData}`;
    } else {
      msg = err.message ? err.message : err.toString();
    }
    console.error(msg);

    return Observable.throw(msg);
  }
}
