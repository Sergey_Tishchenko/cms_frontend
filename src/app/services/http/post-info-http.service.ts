import {Injectable} from '@angular/core';
import {PostInfo} from '../../model/PostInfo';
import {Http, Response, RequestOptions, URLSearchParams} from '@angular/http';

import {config} from '../../../config/config';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {PostInfoService} from "../post-info.service";

@Injectable()
export class PostInfoServiceHttp implements PostInfoService {
  url = config.serverApiUrl + '/postinfos'

  constructor(
    private http: Http
  ) {}

  getAll(limit?: number): Observable<PostInfo[]> {
    let options: RequestOptions = new RequestOptions();
    if (limit) {
      let params = new URLSearchParams();
      params.set('lim', String(limit));
      options.params = params;
    }
    return this.http.get(this.url, options).map(this.extractPosts).catch(this.handleError);
  }


  //helper functions
  private extractPosts(res: Response): PostInfo[] {
    const list: PostInfo[] = [];
    res.json().forEach(item => {
      list.push(
        new PostInfo(item._id, item.title, config.serverUrl + item.iconSrc)
      );
    });
    return list;
  }

  private handleError(err: any, cought: Observable<any>): any {
    let msg = '';
    if (err instanceof Response) {
      const errData = err.json().err || JSON.stringify(err.json());
      msg = `${err.status} - ${err.statusText || ''} ${errData}`;
    } else {
      msg = err.message ? err.message : err.toString();
    }
    console.error(msg);

    return Observable.throw(msg);
  }
}
