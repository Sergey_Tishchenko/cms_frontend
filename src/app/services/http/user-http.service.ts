import {Injectable} from "@angular/core";
import {Http, RequestOptions, Response, Headers} from "@angular/http";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import {config} from "../../../config/config";
import {User} from "../../model/User";
import {Role} from "../../model/Role";
import {UserService} from "../user.service";
import deleteProperty = Reflect.deleteProperty;

@Injectable()
export class UserServiceHttp implements UserService {
  url = config.serverApiUrl + '/users';
  constructor(
    private http: Http
  ) {}

  getAll(): Observable<User[]> {
    return this.http.get(this.url).map(this.extractUsers).catch(this.handleError);
  }

  getUserById(id: number): Observable<User> {
    return this.http.get(this.url + '/' + id).map(this.extractUser).catch(this.handleError);
  }

  create(user: User): Observable<User> {
    console.log('userService create');
    return this.http.post(this.url, user).map(this.extractUser);
  }

  update(user: User): Observable<User> {
    console.log('userService update: ' + this.url + '/' + user.id);
    return this.http.put(this.url + '/' + user.id, user).map(this.extractUser);
  }

  // helper function
  private extractUser(res: Response): User {
    const data = res.json();
    let user = new User(data._id, data.firstName, data.lastName, data.login, data.email, data.pass,
      (data.role) ? new Role(data.role._id, data.role.name) : null);
    return user;
  }

  private extractUsers(res: Response): User[] {
    let users: User[] = [];
    res.json().forEach(data => {
      let user = new User(data._id, data.firstName, data.lastName, data.login, data.email, null,
        (data.role) ? new Role(data.role._id, data.role.name) : null);
      users.push(user);
    });
    return users;
  }

  private handleError(err: any, cought: Observable<any>): any {
    let msg = '';
    if (err instanceof Response) {
      const errData = err.json().err || JSON.stringify(err.json());
      msg = `${err.status} - ${err.statusText || ''} ${errData}`;
    } else {
      msg = err.message ? err.message : err.toString();
    }
    console.error(msg);

    return Observable.throw(msg);
  }
}
