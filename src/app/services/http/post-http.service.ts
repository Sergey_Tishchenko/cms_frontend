import {Injectable} from '@angular/core';

import {Tag} from '../../model/Tag';
import {Http, Response} from '@angular/http';

import {config} from '../../../config/config';
import {Observable} from 'rxjs/Observable';
import {Post} from '../../model/Post';
import {PostService} from "../post.service";
import {User} from "../../model/User";

@Injectable()
export class PostServiceHttp implements PostService {
  url = config.serverApiUrl + '/posts';

  constructor(
    private http: Http
  ) {}

  getPostById(id: string): Observable<Post> {
    return this.http.get(this.url + '/' + id).map(this.extractPost).catch(this.handleError);
  }

  //helper functions
  extractPost(res: Response): Post {
    const data = res.json();
    // console.log(data);
    const post: Post = new Post(data._id, data.title, new Date(data.date), config.serverUrl + data.imgSrc, config.serverUrl + data.iconSrc, data.description, data.content, data.order, [], new User(data.user._id, data.user.firstName, data.user.lastName, data.user.login, null, null, null));
    data.tags.forEach(item => post.tags.push(new Tag(item._id, item.name)));
    return post;
  }

  private handleError(err: any, cought: Observable<any>): any {
    let msg = '';
    if (err instanceof Response) {
      const errData = err.json().err || JSON.stringify(err.json());
      msg = `${err.status} - ${err.statusText || ''} ${errData}`;
    } else {
      msg = err.message ? err.message : err.toString();
    }
    console.error(msg);

    return Observable.throw(msg);
  }
}
