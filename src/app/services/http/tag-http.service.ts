import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import {config} from '../../../config/config';
import {TagService} from "../tag.service";
import {Tag} from '../../model/Tag';

@Injectable()
export class TagServiceHttp implements TagService {
  private url = config.serverApiUrl + '/tags';

  constructor(
    private http: Http
  ) {}

  getAll(): Observable<Tag[]> {
    return this.http.get(this.url).map(this.extractTags).catch(this.handleError);
  }

  getById(id: number): Observable<Tag> {
    return this.http.get(this.url + '/' + id).map(this.extractTag).catch(this.handleError);
  }

  create(tag: Tag): Observable<Tag> {
    console.log('tagService create');
    return this.http.post(this.url, tag).map(this.extractTag);
  }

  update(tag: Tag): Observable<Tag> {
    console.log('tagService update');
    return this.http.put(this.url + '/' + tag.id, tag).map(this.extractTag);
  }

  delete(tag: Tag): Observable<Tag> {
    console.log('tagService delete');
    return this.http.delete(this.url + '/' + tag.id, tag).map(this.extractTag);
  }

  //helper functions
  private extractTags(res: Response): Tag[] {
    const tags: Tag[] = [];
    res.json().forEach(item => {tags.push(new Tag(item._id, item.name)); });
    return tags;
  }

  private extractTag(res: Response): Tag {
    const data = res.json();
    return new Tag(data._id, data.name);
  }

  private handleError(err: any, cought: Observable<any>): any {
    let msg = '';
    if (err instanceof Response) {
      const errData = err.json().err || JSON.stringify(err.json());
      msg = `${err.status} - ${err.statusText || ''} ${errData}`;
    } else {
      msg = err.message ? err.message : err.toString();
    }
    console.error(msg);

    return Observable.throw(msg);
  }

}
