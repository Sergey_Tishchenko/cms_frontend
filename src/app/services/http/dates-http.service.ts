import {Injectable} from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {Http, Response, RequestOptions, URLSearchParams} from '@angular/http';

import {config} from '../../../config/config';
import {DatesService} from "../dates.service";

@Injectable()
export class DatesServiceHttp implements DatesService {
  url = config.serverApiUrl + '/dates';

  constructor(
    private http: Http
  ) {}

  getAll(limit?: number): Observable<Date[]> {
    let options: RequestOptions = new RequestOptions();
    if (limit) {
      let params = new URLSearchParams();
      params.set('lim', String(limit));
      options.params = params;
    }
    return this.http.get(this.url, options).map(this.extractDates).catch(this.handleError);
  }

  //helper functions
  extractDates(res: Response) {
    // console.log(res.json());
    const list: Date[] = [];
    res.json().forEach(item => {list.push(new Date(item.date)); });
    // console.log(list);
    return list;
  }

  private handleError(err: any, cought: Observable<any>): any {
    let msg = '';
    if (err instanceof Response) {
      const errData = err.json().err || JSON.stringify(err.json());
      msg = `${err.status} - ${err.statusText || ''} ${errData}`;
    } else {
      msg = err.message ? err.message : err.toString();
    }
    console.error(msg);

    return Observable.throw(msg);
  }
}
