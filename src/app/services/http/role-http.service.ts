import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import {Http, Response} from "@angular/http";

import {Role} from "../../model/Role";
import {config} from "../../../config/config";

@Injectable()
export class RoleServiceHttp {
  url = config.serverApiUrl + '/roles'
  constructor(
    private http: Http
  ) {}
  getAll(): Observable<Role[]> {
    return this.http.get(this.url).map(this.extractRoles).catch(this.handleError);
  }

  private extractRoles(res: Response): Role[] {
    let roles = [];
    res.json().forEach(item => roles.push(new Role(item._id, item.name)));
    return roles;
  }

  private handleError(err: any, cought: Observable<any>): any {
    let msg = '';
    if (err instanceof Response) {
      const errData = err.json().err || JSON.stringify(err.json());
      msg = `${err.status} - ${err.statusText || ''} ${errData}`;
    } else {
      msg = err.message ? err.message : err.toString();
    }
    console.error(msg);

    return Observable.throw(msg);
  }
}
