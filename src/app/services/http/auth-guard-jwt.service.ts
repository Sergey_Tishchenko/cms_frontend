import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from "@angular/router";
import {AuthGuard} from "../auth-guard.service";
import {AuthService} from "../auth/auth.service";

@Injectable()
export class AuthGuardJWT implements AuthGuard {

  constructor(
    private router: Router,
    private authService: AuthService
  ) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (this.authService.isLoggedIn()) {
      console.log('AuthGuardJWT: allowed');
      return true;
    } else {
      console.log('AuthGuardJWT: forbidden');
      this.router.navigate(['/login']);
      return false;
    }
  }

}
