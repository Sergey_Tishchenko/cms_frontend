import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {config} from "../../../config/config";
import {JwtHelper} from 'angular2-jwt';
import {Observable} from "rxjs/Observable";
import {Observer} from "rxjs/Observer";
import {User} from "../../model/User";
import {Role} from "../../model/Role";

@Injectable()
export class AuthService {
  url = config.serverApiUrl + '/authenticate';
  constructor(
    private http: Http
  ) {}

  login(user: {login: any, pass: any}): Observable<boolean> {
    return Observable.create((observer: Observer<boolean>) => {

      this.http.post(this.url, user)
        .subscribe(res => {
          // console.log(res.json());
          let token = res.json().token;
          let curUser = res.json().user;
          if (token) {
            localStorage.setItem('token', token);
            localStorage.setItem('user', JSON.stringify(curUser));

            // let jwtHelper = new JwtHelper();
            // console.log(jwtHelper.decodeToken(token));
            // console.log(curUser);

            observer.next(true);
            observer.complete();

          } else {
            console.log("no token");
            observer.error(new Error("no token"));
            observer.complete();
          }
        }, err => {
          console.log(err);
          observer.error(err);
          observer.complete();
        }
      );
    });
  }

  logout() {
    console.log('logout');
    localStorage.removeItem('token');
    localStorage.removeItem('user');
  }

  isLoggedIn(): boolean {
    let token = localStorage.getItem('token');
    if (token) {
      let jwtHelper = new JwtHelper();
      // return jwtHelper.isTokenExpired(token);
      console.log('logged in');
      return true;
    } else {
      console.log('not logged in');
      return false;
    }
  }

  getCurrentUser(): User {
    let user = JSON.parse(localStorage.getItem('user'));
    return new User(user._id, user.firstName, user.lastName, user.login, user.email, user.pass, new Role(user.role._id, user.role.name));
  }
}
