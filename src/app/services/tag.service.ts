import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';

import {Tag} from "../model/Tag";


@Injectable()
export abstract class TagService {
  abstract getAll(): Observable<Tag[]>;
  abstract getById(id: number): Observable<Tag>;
  abstract create(tag: Tag): Observable<Tag>;
  abstract update(tag: Tag): Observable<Tag>;
  abstract delete(tag: Tag): Observable<Tag>;
}
