import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import {User} from "../model/User";

@Injectable()
export abstract class UserService {

  abstract getAll(): Observable<User[]>;
  abstract getUserById(id: number): Observable<User>;
  abstract update(user: User): Observable<User>;
  abstract create(user: User): Observable<User>;
}
