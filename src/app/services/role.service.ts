import {Injectable} from "@angular/core";
import {Role} from "../model/Role";
import {Observable} from "rxjs/Observable";

@Injectable()
export abstract class RoleService {
  abstract getAll(): Observable<Role[]>;
}
