import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';

import {PostInfo} from "../model/PostInfo";

@Injectable()
export abstract class PostInfoService {
  abstract getAll(limit?: number): Observable<PostInfo[]>;
}
