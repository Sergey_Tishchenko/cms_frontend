import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';

import {Post} from "../model/Post";

@Injectable()
export abstract class PostService {
  abstract getPostById(id: string): Observable<Post>;
}
