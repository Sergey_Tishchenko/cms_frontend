import {Injectable} from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';

@Injectable()
export class DatesServiceMock {
  private dates: Date[] = [
    new Date(2017, 7, 5),
    new Date(2017, 5, 9),
    new Date(2017, 2, 8),
    new Date(2016, 10, 25),
    new Date(2017, 8, 26),
  ];

  getAll(limit?: number): Observable<Date[]> {
    return Observable.create((observer: Observer<Date[]>) => {
      observer.next(this.dates);
      observer.complete();
    });
  }
}
