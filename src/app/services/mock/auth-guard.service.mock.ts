import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from "@angular/router";
import {AuthGuard} from "../auth-guard.service";

@Injectable()
export class AuthGuardMock implements AuthGuard {
  constructor(
    private router: Router
  ) {}
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    console.log('AuthGuard: access allowed');
    this.router.navigate(['/login']);
    return false;
  }

}
