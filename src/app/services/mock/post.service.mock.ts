import {Injectable} from '@angular/core';
import {Tag} from '../../model/Tag';
import {Observable} from 'rxjs/Observable';
import {Post} from '../../model/Post';

@Injectable()
export class PostServiceMock {
  // posts: PostContent[] = [
  //   new PostContent('1', ' Post title 1', 12345, '../../assets/img/large_1.jpg', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet atque consequuntur deleniti distinctio dolor dolore ea eum fuga harum illo laudantium maxime mollitia nihil optio pariatur quas quasi quis, voluptas?', [new Tag('1', 'Autos')]),
  //   new PostContent('2', 'Post title 2', 12345, '../../assets/img/large_2.jpg', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet atque consequuntur deleniti distinctio dolor dolore ea eum fuga harum illo laudantium maxime mollitia nihil optio pariatur quas quasi quis, voluptas?', [new Tag('1', 'Autos'), new Tag('2', 'Future')]),
  //   new PostContent('3', 'Post title 3', 12345, '../../assets/img/large_3.jpg', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet atque consequuntur deleniti distinctio dolor dolore ea eum fuga harum illo laudantium maxime mollitia nihil optio pariatur quas quasi quis, voluptas?', [new Tag('2', 'Future'), new Tag('3', 'Science')]),
  //   new PostContent('4', 'Post title 4', 12345, '../../assets/img/large_4.jpg', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet atque consequuntur deleniti distinctio dolor dolore ea eum fuga harum illo laudantium maxime mollitia nihil optio pariatur quas quasi quis, voluptas?', [new Tag('3', 'Science')])
  // ];

  getPostById(id: string): Observable<Post> {
    return Observable.create();
  }
}
