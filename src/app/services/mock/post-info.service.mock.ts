import {Injectable} from '@angular/core';
import {PostInfo} from '../../model/PostInfo';
import {Observable} from 'rxjs/Observable';
import {Observer} from 'rxjs/Observer';

@Injectable()
export class PostInfoServiceMock {
  private postInfoList: PostInfo[] = [
    new PostInfo('1', 'title 1', '../../assets/img/small_1.jpg'),
    new PostInfo('2', 'title 2', '../../assets/img/small_2.jpg'),
    new PostInfo('3', 'title 3', '../../assets/img/small_3.jpg'),
    new PostInfo('4', 'title 4', '../../assets/img/small_4.jpg'),
  ];

  getAll(limit?: number): Observable<PostInfo[]> {
    return Observable.create((observer: Observer<PostInfo[]>) => {
      observer.next(this.postInfoList);
      observer.complete();
    });
  }
}
