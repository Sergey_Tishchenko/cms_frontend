import {Injectable} from '@angular/core';
import {Tag} from '../../model/Tag';
import {Observable} from 'rxjs/Observable';
import {Observer} from 'rxjs/Observer';

@Injectable()
export class TagServiceMock {
  private tagList = [
    new Tag('1', 'Autos'),
    new Tag('2', 'Earth'),
    new Tag('3', 'Sport'),
    new Tag('4', 'Future'),
  ];

  getAll(): Observable<Tag[]> {
    return Observable.create((observer: Observer<Tag[]>) => {
      observer.next(this.tagList);
      observer.complete();
    });
  }
}
