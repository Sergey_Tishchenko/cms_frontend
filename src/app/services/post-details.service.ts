import {Injectable} from '@angular/core';
import {PostDetails} from '../model/PostDetails';
import {Observable} from 'rxjs/Observable';

@Injectable()
export abstract class PostDetailsService {
  abstract getByTagId(id: string): Observable<PostDetails[]>;
  abstract getForHome(): Observable<PostDetails[]>;
  abstract getForArchive(): Observable<PostDetails[]>;
  abstract getForArchiveWithDate(date: string): Observable<PostDetails[]>;
  abstract getAll(): Observable<PostDetails[]>;
}
