import {Injectable} from '@angular/core';
import { Observable } from 'rxjs/Observable';

@Injectable()
export abstract class DatesService {
  abstract getAll(limit?: number): Observable<Date[]>;
}
