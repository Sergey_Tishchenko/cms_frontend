import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { TopNavComponent } from './components/top-nav/top-nav.component';
import { AppStatesModule } from './states/app-states.module';
import {PostInfoServiceHttp} from './services/http/post-info-http.service';
import {TagServiceHttp} from './services/http/tag-http.service';
import {DatesServiceHttp} from './services/http/dates-http.service';
import {PostDetailsServiceHttp} from "./services/http/post-details-http.service";
import {PostDetailsService} from "./services/post-details.service";
import {PostService} from "./services/post.service";
import {DatesService} from "./services/dates.service";
import {PostServiceHttp} from "./services/http/post-http.service";
import {PostInfoService} from "./services/post-info.service";
import {TagService} from "./services/tag.service";
import {AuthGuard} from "./services/auth-guard.service";
import {AuthGuardMock} from "./services/mock/auth-guard.service.mock";
import {UserService} from "./services/user.service";
import {UserServiceHttp} from "./services/http/user-http.service";
import {RoleService} from "./services/role.service";
import {RoleServiceHttp} from "./services/http/role-http.service";
import {AuthService} from "./services/auth/auth.service";
import {AuthGuardJWT} from "./services/http/auth-guard-jwt.service";
import {JasperoAlertsModule} from "./lib/jaspero-alerts/alerts.module";


@NgModule({
  declarations: [
    AppComponent,
    TopNavComponent
  ],
  imports: [
    JasperoAlertsModule,
    BrowserModule,
    HttpModule,
    AppStatesModule,
    RouterModule.forRoot([
      // {path: '', redirectTo: '/main', pathMatch: 'full'}
    ])
  ],
  providers: [
    {provide: DatesService, useClass: DatesServiceHttp},
    {provide: PostService, useClass: PostServiceHttp},
    {provide: PostDetailsService, useClass: PostDetailsServiceHttp},
    {provide: PostInfoService, useClass: PostInfoServiceHttp},
    {provide: TagService, useClass: TagServiceHttp},
    {provide: UserService, useClass: UserServiceHttp},
    {provide: RoleService, useClass: RoleServiceHttp},
    {provide: AuthService, useClass: AuthService},
    {provide: AuthGuard, useClass: AuthGuardJWT},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
