import {Tag} from './Tag';
import {User} from "./User";

export class Post {
  constructor(
    public id: string,
    public title: string,
    public date: Date,
    public imgSrc: string,
    public iconSrc: string,
    public description: string,
    public content: string,
    public order: number,
    public tags: Tag[],
    public user: User
  ) {}
}
