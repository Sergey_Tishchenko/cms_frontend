import {User} from "./User";

export class PostDetails {
  constructor(
    public id: string,
    public title: string,
    public date: Date,
    public imgSrc: string,
    public iconSrc: string,
    public description: string,
    public user: User
  ) {}
}
