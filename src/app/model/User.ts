import {Role} from "./Role";

export class User {
  constructor(
    public id: string,
    public firstName: string,
    public lastName: string,
    public login: string,
    public email: string,
    public pass: string,
    public role: Role
  ) {}

  toJSON()  {
    return {
      id: this.id,
      firstName: this.firstName,
      lastName: this.lastName,
      login: this.login,
      email: this.email,
      pass: this.pass,
      role: this.role.id
    };
  }
}
