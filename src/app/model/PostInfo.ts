export class PostInfo {
  constructor(
    public id: string,
    public title: string,
    public iconSrc: string
  ){}
}
