import { CMSPage } from './app.po';

describe('cms App', () => {
  let page: CMSPage;

  beforeEach(() => {
    page = new CMSPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
